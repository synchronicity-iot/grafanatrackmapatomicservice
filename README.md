# Grafana trackmap plugin Atomic Service

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

> Grafana Trackmap Atomic Service provides an easy way to visualize data from a time series datasource with NGSIv2 format

This service is composed by Grafana, and [Alexandra-Trackmap-Panel](https://grafana.com/grafana/plugins/alexandra-trackmap-panel) for Grafana.  
Grafana provides a user management system allowing the creation and management of isolated dashboards with custom charts and device information.  
The Trackmap panel is a plugin for Grafana, that visualize geo:json or NGSIv2 on a map as either Ant-path, Hexbin or Heatmap.

![Track Map Image](./images/trackMap.jpg)

To use this service a time series database need to be setup already.

## Deployment

To run the atomic service the following is required:
  - docker
  - docker-compose

Launch atomic service:
```
docker-compose up
```

or launch the atomic service in the background:
```
docker-compose up -d
```

To check the state of the containers:
```
docker-compose ps
```

Stop the services with:
```
docker-compose stop
```

## Usage

Once the service it up and running. Is it accessible on port 3000.  
Open your browser on `localhost:3000` to view Grafana.  
The default login is:
- **username**: admin
- **password**: admin

### Datasource

To use this service you need to connect to a datasource. To add a CrateDB as a datasource, go to `Add data source`, select PostgreSQL and fill in the fields:
To use a CrateDB as a data source,
  - **Name**: This is the name you want to use for the datasource.
  - **Host**: The full url where CrateDB is accessible, but on port `5432`.
  - **Database**: This is the name of the database schema. By default crate uses doc scheema.
  - **User**: Use `crate`.
  - **SSL Mode**: set to `disable`.

Click *Save and Test* and you should get an OK message.

### Trackmap panel

Create a Dashboard, and choose visualization. In the list of visualization select `Track Map`.  
A guide to all the settings for the Track Map can be found [here](https://github.com/alexandrainst/alexandra-trackmap-panel#usage)

Setup the query by selecting the datasource you just added (**1**). Set the *Format as* to `Table` (**2**), and toggle text edit mode on (**3**). See the image below for reference:  
![Query example](./images/query.jpg)

A example of a query to use:
```sql
SELECT time_index, location
FROM doc.table_name
WHERE $__timeFilter(time_index)
ORDER BY time_index
```

More query examples can be found in the guide linked to above.

## License
This atomic service uses Grafana and a trackmap plugin for Grafana.

Grafana license: [Apache License © Grafana](https://github.com/grafana/grafana/blob/master/LICENSE)  
Trackmap plugin license: [MIT © Alexandra Institute.](https://github.com/alexandrainst/alexandra-trackmap-panel/blob/master/LICENSE)  
